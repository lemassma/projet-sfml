
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <math.h>
#include <stdbool.h>
#define LARG_SHIP 64
#define HAUT_SHIP 54
#define LARG_FENETRE 1600
#define HAUT_FENETRE 900
#define SHIP_SPEED 4.5
#define ANGLE_INCREMENT 3

#define POSITION_X_ECRAN_0 -20
#define POSITION_Y_ECRAN_0 -10

#define POSITION_X_ECRAN_1 -240
#define POSITION_Y_ECRAN_1 -1360

#define POSITION_X_ECRAN_2 -20
#define POSITION_Y_ECRAN_2 -2485

#define POSITION_X_ECRAN_3 -2020
#define POSITION_Y_ECRAN_3 -2740

#define POSITION_X_ECRAN_4 -2300
#define POSITION_Y_ECRAN_4 -1260

#define POSITION_X_ECRAN_5 -2000
#define POSITION_Y_ECRAN_5 -13




using namespace sf;


float DegreToRad(float degree);
int avance = 0;
int tourne = 0;
float rotation;
float rotationTir;
float direction_x;
float direction_y;
float directionT_x;
float directionT_y;


Vector2f shipS_position;



int main()
{


    bool isShooting = false;

    RenderWindow fenetre(VideoMode(LARG_FENETRE, HAUT_FENETRE), "Tirs!");
    fenetre.setFramerateLimit(70);


    Texture tir1T;
    Texture shipT;


    if (!shipT.loadFromFile("spaceship.png"))
        printf("PB de chargement de l'image !\n");
    Sprite shipS(shipT) ;
    shipS.setOrigin(LARG_SHIP/2,HAUT_SHIP/2);
    shipS.setPosition(LARG_FENETRE/2,HAUT_FENETRE/2);


    if (!tir1T.loadFromFile("missile.png"))
        printf("PB de chargement de l'image !\n");
    Sprite tir1S(tir1T) ;
    tir1S.setOrigin(shipS.getOrigin().x,shipS.getOrigin().y);
    tir1S.setScale(0.1,0.1);


    Texture texture_0;
    if (!texture_0.loadFromFile("collage.jpg"))
        return EXIT_FAILURE;
    Sprite background(texture_0);
    background.setPosition(0,0);





    while(fenetre.isOpen())
    {
        shipS_position = shipS.getPosition();
        rotation = shipS.getRotation();
        rotationTir=tir1S.getRotation();
        Event event;
        while (fenetre.pollEvent(event))
        {
            if (rotation <=90)
            {
                direction_x = sin(DegreToRad(rotation))*SHIP_SPEED;
                direction_y = -cos(DegreToRad(rotation))*SHIP_SPEED;

            }

            else if (rotation <=180)
            {
                direction_x = cos(DegreToRad(rotation-90))*SHIP_SPEED;
                direction_y = sin(DegreToRad(rotation-90))*SHIP_SPEED;

            }

            else if (rotation <=270)
            {
                direction_x = -sin(DegreToRad(rotation-180))*SHIP_SPEED;
                direction_y = cos(DegreToRad(rotation-180))*SHIP_SPEED;

            }

            else if (rotation <=360)
            {
                direction_x = -cos(DegreToRad(rotation-270))*SHIP_SPEED;
                direction_y = -sin(DegreToRad(rotation-270))*SHIP_SPEED;

            }







            if (rotationTir <=90)
            {
                directionT_x = sin(DegreToRad(rotationTir))*SHIP_SPEED;
                directionT_y = -cos(DegreToRad(rotationTir))*SHIP_SPEED;


            }

            else if (rotationTir <=180)
            {
                directionT_x = cos(DegreToRad(rotationTir-90))*SHIP_SPEED;
                directionT_y = sin(DegreToRad(rotationTir-90))*SHIP_SPEED;

            }

            else if (rotationTir <=270)
            {
                directionT_x = -sin(DegreToRad(rotationTir-180))*SHIP_SPEED;
                directionT_y = cos(DegreToRad(rotationTir-180))*SHIP_SPEED;

            }

            else if (rotationTir <=360)
            {
                directionT_x = -cos(DegreToRad(rotationTir-270))*SHIP_SPEED;
                directionT_y = -sin(DegreToRad(rotationTir-270))*SHIP_SPEED;

            }



            switch (event.type)
            {

            case Event::Closed:
                fenetre.close();
                break;

            case Event::KeyPressed:
                if(event.key.code==Keyboard::Space)
                {
                    isShooting =true;




                }

                if (event.key.code== Keyboard::Escape)
                    fenetre.close();





                if ( event.key.code == Keyboard::Up)
                {

                        avance = 1;

                }


                if ( event.key.code == Keyboard::Left)
                {

                    tourne = -1;


                }

                else   if ( event.key.code == Keyboard::Right)
                {

                    tourne = 1;

                }


                break;


            case Event::KeyReleased:
            {
                if (event.key.code == Keyboard::Up)

                    avance = 0;
                if (event.key.code == Keyboard::Right)

                    tourne = 0;
                if (event.key.code == Keyboard::Left)

                    tourne = 0;
            }
            break;
            }

        }

        if(shipS_position.x  +15<= 0)
        {
            shipS.setPosition(LARG_FENETRE,shipS_position.y);

        }

        if(shipS_position.x >= LARG_FENETRE)
        {
            shipS.setPosition(1, shipS_position.y);

        }

        if(shipS_position.y <= 0)
        {
            shipS.setPosition(shipS_position.x,HAUT_FENETRE);
        }


        if(shipS_position.y -5>= HAUT_FENETRE)
        {
            shipS.setPosition(shipS_position.x, 1);
        }

        if(tir1S.getPosition().y<=0|| tir1S.getPosition().y>=HAUT_FENETRE || tir1S.getPosition().x<=0 || tir1S.getPosition().x>=LARG_FENETRE)
            {
                tirHorsFen=1;
            }
        else
                tirHorsFen=0;

        if(avance==1)
            shipS.move(direction_x,direction_y);

        if(tourne==1)
            shipS.rotate(ANGLE_INCREMENT);
        else if(tourne==-1)
            shipS.rotate(-ANGLE_INCREMENT);


        if(isShooting)
        {
            tir1S.move(directionT_x*4,directionT_y*4);

        }

        if((avance==1 || tourne==1 || tourne==-1) && tirHorsFen==1)
        {

            isShooting=false;
            tir1S.setPosition(shipS.getPosition().x,shipS.getPosition().y);


            if(avance==1)
                tir1S.setRotation(shipS.getRotation());
            if(tourne==1)
                tir1S.setRotation(shipS.getRotation());
            else if(tourne==-1)
                tir1S.setRotation(shipS.getRotation());


        }



        if ((tir1S.getPosition().y<=0 || tir1S.getPosition().y>=HAUT_FENETRE) && (tir1S.getPosition().x<=0 || tir1S.getPosition().x>=LARG_FENETRE))
        {
            isShooting=false;
            tir1S.setPosition(shipS.getPosition().x,shipS.getPosition().y);
            if(avance==1)
                shipS.move(direction_x,direction_y);
            if(tourne==1)
                shipS.rotate(ANGLE_INCREMENT);
            else if(tourne==-1)
                shipS.rotate(-ANGLE_INCREMENT);

        }


        if((avance==1 || tourne==1 || tourne==-1) && !(isShooting))
        {
            tir1S.setPosition(shipS.getPosition().x,shipS.getPosition().y);
            if(tourne==1)
                tir1S.rotate(ANGLE_INCREMENT);
            else if(tourne==-1)
                tir1S.rotate(-ANGLE_INCREMENT);

        }


        background.setPosition(-2020,-13);


        float rotShip=shipS.getRotation();
        float rotTir=tir1S.getRotation();

        printf("%f   %f\n",rotShip,rotTir);


        fenetre.clear();
        fenetre.draw(background);
        fenetre.draw(shipS);
        fenetre.draw(tir1S);
        fenetre.display();
    }

    return EXIT_SUCCESS;

}

float DegreToRad(float degree)
{
    float radian;
    return  radian = (degree * M_PI)/180;
}
