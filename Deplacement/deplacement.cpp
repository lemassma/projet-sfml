#include <SFML/Graphics.hpp>
#include <math.h>

#define LARG_SHIP 90
#define HAUT_SHIP 100
#define LARG_FENETRE 1600
#define HAUT_FENETRE 900

using namespace sf;



int main()
{

    RenderWindow app(sf::VideoMode(LARG_FENETRE, HAUT_FENETRE), "TEST_DEPLACEMENT");

    //Chargement des textures
    Texture texture_backg;
    if (!texture_backg.loadFromFile("background.jpg"))
        return EXIT_FAILURE;
    Sprite background(texture_backg);

    Texture texture_ship;
    if (!texture_ship.loadFromFile("spaceship.png"))
        return EXIT_FAILURE;
    Sprite ship(texture_ship);
    ship.setOrigin(LARG_SHIP/2,HAUT_SHIP/2);
    ship.setPosition(LARG_FENETRE/2,HAUT_FENETRE/2);
    float dx=0,dy=0,angle=0;
            bool turbo;
    //Evenement (Controles)
    while (app.isOpen())
    {

        Event event;
        while (app.pollEvent(event))
        {



            if(Keyboard::isKeyPressed(Keyboard::Right))
                angle+=0.5;

            if(Keyboard::isKeyPressed(Keyboard::Left))
                angle-=0.5;

            if(Keyboard::isKeyPressed(Keyboard::Up))
                turbo=true;
            else
                turbo=false;


            if (turbo)
            {
                dx+=cos(angle)*0.2;
                dy*=sin(angle)*0.2;
            }
            else{
                dx*=0.99;
                dy*=0.99;}



        app.clear();
        app.draw(background);
        app.draw(ship);
        app.display();
    }



}

return EXIT_SUCCESS;
}

