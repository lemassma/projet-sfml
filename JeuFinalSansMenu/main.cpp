#include <SFML/Graphics.hpp>
#include <math.h>
#include <stdbool.h>
#include "test.cpp"

#define LARG_SHIP 64
#define HAUT_SHIP 54
#define LARG_FENETRE 1700
#define HAUT_FENETRE 1000
#define SHIP_SPEED 4.5
#define ANGLE_INCREMENT 3
#define valeur_score 50

#define POSITION_X_ECRAN_0 -20
#define POSITION_Y_ECRAN_0 -10

#define POSITION_X_ECRAN_1 -240
#define POSITION_Y_ECRAN_1 -1360

#define POSITION_X_ECRAN_2 -20
#define POSITION_Y_ECRAN_2 -2485

#define POSITION_X_ECRAN_3 -2020
#define POSITION_Y_ECRAN_3 -2740

#define POSITION_X_ECRAN_4 -2300
#define POSITION_Y_ECRAN_4 -1260

#define POSITION_X_ECRAN_5 -2000
#define POSITION_Y_ECRAN_5 -13


using namespace sf;



float DegreToRad(float degree);
int avance = 0;
int tourne = 0;
float rotation;
float rotationTir;
float direction_x;
float direction_y;
float directionT_x;
float directionT_y;
float x,x2,x3,x4,x5,x6;
float y,y2,y3,y4,y5,y6;
int vie=3;
float tirHorsFen=0;
bool isShooting = false;
float score=0;



Vector2f ship_position;

int main()
{

    RenderWindow app(sf::VideoMode(LARG_FENETRE, HAUT_FENETRE), "TEST_DEPLACEMENT");
    app.setFramerateLimit(70);

//--TEXTURE BACKROUND----------------------------------------------------------------
    Texture texture_0;
    if (!texture_0.loadFromFile("collage.jpg"))
        return EXIT_FAILURE;
    Sprite background(texture_0);
    background.setPosition(0,0);



//--TEXTURE VAISSEAU------------------------------------------------------------------
    Texture texture_ship;
    if (!texture_ship.loadFromFile("spaceship.png"))
        return EXIT_FAILURE;
    Sprite ship;
    ship.setTexture(texture_ship);
    Color original2 = ship.getColor();
    ship.setOrigin(LARG_SHIP/2,HAUT_SHIP/2);
    ship.setPosition(LARG_FENETRE/2,HAUT_FENETRE/2);



//--TEXTURE TIR------------------------------------------------------------------------
    Texture tir1T;
    if (!tir1T.loadFromFile("missile.png"))
        printf("PB de chargement de l'image !\n");
    Sprite tir1S(tir1T) ;
    tir1S.setOrigin(ship.getOrigin().x,ship.getOrigin().y);
    tir1S.setScale(0.1,0.1);

//--TEXTURE METEORE--------------------------------------------------------------------

    Texture image;
    if (!image.loadFromFile("comete.png"))
        printf("Pb chargement de l'image");

//meteore num�ro 1
    Sprite meteore;
    meteore.setTexture(image);
    Color original = meteore.getColor();
    meteore.setScale(0.6f, 0.6f);
    x=rand()%HAUT_FENETRE;
    y=rand()%LARG_FENETRE;
    meteore.setPosition(x,y);



//meteore numero 2
    Sprite meteore2;
    meteore2.setTexture(image);
    meteore2.setScale(0.8f, 0.8f);
    x2=rand()%HAUT_FENETRE;
    y2=rand()%LARG_FENETRE;
    meteore2.setPosition(x2,y2);


//meteore numero 3
    Sprite meteore3;
    meteore3.setTexture(image);
    meteore3.setScale(1.0f, 1.0f);
    x3=0;
    y3=0;
    meteore3.setPosition(x3,y3);


//meteore numero 4
    Sprite meteore4;
    meteore4.setTexture(image);
    meteore4.setScale(0.9f, 0.9f);
    x4=rand()%HAUT_FENETRE;
    y4=rand()%LARG_FENETRE;
    meteore4.setPosition(x4,y4);

//meteore numero 5
    Sprite meteore5;
    meteore5.setTexture(image);
    meteore5.setScale(0.8f, 0.8f);
    x5=rand()%HAUT_FENETRE;
    y5=rand()%LARG_FENETRE;
    meteore5.setPosition(x5,y5);

//meteore numero 6
    Sprite meteore6;
    meteore6.setTexture(image);
    meteore6.setScale(1.1f, 1.1f);
    x6=rand()%HAUT_FENETRE;
    y6=rand()%LARG_FENETRE;
    meteore6.setPosition(x6,y6);


//--EVENEMENTS--------------------------------------------------------------------------------------------

    app.setFramerateLimit(70);

    while (app.isOpen())
    {
        ship_position = ship.getPosition();
        rotation = ship.getRotation();
        rotationTir=tir1S.getRotation();
        Event event;
        while (app.pollEvent(event))
        {
//---------------------------------------------------------------------------------------------------------
            //Cr�ation de la direction : A transformer en fonction pour alleger


            if (rotation <=90)
            {
                direction_x = sin(DegreToRad(rotation))*SHIP_SPEED;
                direction_y = -cos(DegreToRad(rotation))*SHIP_SPEED;
            }

            else if (rotation <=180)
            {
                direction_x = cos(DegreToRad(rotation-90))*SHIP_SPEED;
                direction_y = sin(DegreToRad(rotation-90))*SHIP_SPEED;
            }

            else if (rotation <=270)
            {
                direction_x = -sin(DegreToRad(rotation-180))*SHIP_SPEED;
                direction_y = cos(DegreToRad(rotation-180))*SHIP_SPEED;
            }

            else if (rotation <=360)
            {
                direction_x = -cos(DegreToRad(rotation-270))*SHIP_SPEED;
                direction_y = -sin(DegreToRad(rotation-270))*SHIP_SPEED;
            }





            if (rotationTir <=90)
            {
                directionT_x = sin(DegreToRad(rotationTir))*SHIP_SPEED;
                directionT_y = -cos(DegreToRad(rotationTir))*SHIP_SPEED;


            }

            else if (rotationTir <=180)
            {
                directionT_x = cos(DegreToRad(rotationTir-90))*SHIP_SPEED;
                directionT_y = sin(DegreToRad(rotationTir-90))*SHIP_SPEED;

            }

            else if (rotationTir <=270)
            {
                directionT_x = -sin(DegreToRad(rotationTir-180))*SHIP_SPEED;
                directionT_y = cos(DegreToRad(rotationTir-180))*SHIP_SPEED;

            }

            else if (rotationTir <=360)
            {
                directionT_x = -cos(DegreToRad(rotationTir-270))*SHIP_SPEED;
                directionT_y = -sin(DegreToRad(rotationTir-270))*SHIP_SPEED;

            }

//-----------------------------------------------------------------------------------


            if (event.type==Event::Closed)
                app.close();


            if (event.type == Event::KeyPressed)
            {
                if(event.key.code==Keyboard::Space)
                {
                    isShooting =true;

                }

                if ( event.key.code == Keyboard::Up)
                {
                    avance = 1;
                    //ship.move(direction_x,direction_y);
                }


                if ( event.key.code == Keyboard::Left)
                {
                    tourne = -1;
                    // ship.rotate(-ANGLE_INCREMENT);

                }

                else   if ( event.key.code == Keyboard::Right)
                {
                    tourne = 1;
                    //ship.rotate(ANGLE_INCREMENT);
                }


                if (event.key.code == Keyboard::Escape)
                    app.close();

            }
            if (event.type == Event::KeyReleased)
            {
                if (event.key.code == Keyboard::Up)
                    avance = 0;
                if (event.key.code == Keyboard::Right)
                    tourne = 0;
                if (event.key.code == Keyboard::Left)
                    tourne = 0;
            }



        }

//--DELIMITATION FENETRE-----------------------------------------------

        if(ship_position.x  +15<= 0)
        {
            ship.setPosition(LARG_FENETRE,ship_position.y);
            //change_texture = -1;
        }

        if(ship_position.x >= LARG_FENETRE)
        {
            ship.setPosition(1, ship_position.y);
            //change_texture = 1;
        }

        if(ship_position.y <= 0)
            ship.setPosition(ship_position.x,HAUT_FENETRE);

        if(ship_position.y -5>= HAUT_FENETRE)
            ship.setPosition(ship_position.x, 1);



        if(tir1S.getPosition().y<=0|| tir1S.getPosition().y>=HAUT_FENETRE || tir1S.getPosition().x<=0 || tir1S.getPosition().x>=LARG_FENETRE)
        {
            tirHorsFen=1;

        }
        else
            tirHorsFen=0;






//--BOOL----------------------------------------------------------------

        if(avance==1)
            ship.move(direction_x,direction_y);

        if(tourne==1)
            ship.rotate(ANGLE_INCREMENT);
        else if(tourne==-1)
            ship.rotate(-ANGLE_INCREMENT);







        if(isShooting)
        {
            tir1S.move(directionT_x*4,directionT_y*4);

        }









//SI LE TIR EST HORS FENETRE EN X OU EN Y
        if ((tir1S.getPosition().y<=0 || tir1S.getPosition().y>=HAUT_FENETRE) || (tir1S.getPosition().x<=0 || tir1S.getPosition().x>=LARG_FENETRE))
        {
            isShooting=false;
            tir1S.setPosition(ship.getPosition().x,ship.getPosition().y);
            if(avance==1)
                ship.move(direction_x,direction_y);
            if(tourne==1)
                ship.rotate(ANGLE_INCREMENT);
            else if(tourne==-1)
                ship.rotate(-ANGLE_INCREMENT);
                tir1S.setRotation(ship.getRotation());

        }

//SI LE VAISSEAU SE DEPLACE ET NE TIR PAS
        if((avance==1 || tourne==1 || tourne==-1) && !(isShooting))

        {

            tir1S.setPosition(ship.getPosition().x,ship.getPosition().y);
            if(tourne==1)
                tir1S.rotate(ANGLE_INCREMENT);
            else if(tourne==-1)
                tir1S.rotate(-ANGLE_INCREMENT);

        }

        //SI LE VAISSEAU SE DEPLACE ET LE TIR ET HORS DE LA FENETRE


if((avance==1 || tourne==1 || tourne==-1) && tirHorsFen==1)
        {

            isShooting=false;
            tir1S.setPosition(ship.getPosition().x,ship.getPosition().y);
            tir1S.setRotation(ship.getRotation());
        }

//--METEORE-------------------------------------------------------------
        x=x+3;
        y2=y2+2;
        x3=x3+3;
        y3=y3+2;
        x4=x4+2;
        y4=y4-2;
        x5=x5-2;
        y5=y5+2;
        x6=x6-2;
        y6=y6-2;
        meteore.setPosition(x,y);
        meteore2.setPosition(x2,y2);
        meteore3.setPosition(x3,y3);
        meteore4.setPosition(x4,y4);
        meteore5.setPosition(x5,y5);
        meteore6.setPosition(x6,y6);
        //quand la com�te sors de l'�cran
        if (x>=LARG_FENETRE)
        {
            x=0;
            y=rand()%HAUT_FENETRE;
        }
        if(y2>HAUT_FENETRE)
        {
            y2=0;
            x2=rand()%LARG_FENETRE;
        }
        if (x3>LARG_FENETRE || y3>HAUT_FENETRE)
        {
            x3=rand()%LARG_FENETRE;
            y3=0;
        }
        if(x4<0 || y4<=0)
        {
            x4=rand()%LARG_FENETRE;
            y4=HAUT_FENETRE;
        }
        if (y5>HAUT_FENETRE)
        {
            x5=rand()%LARG_FENETRE;
            y5=0;
        }
        if (y6<0)
        {
            y6=HAUT_FENETRE;
            x6=rand()%LARG_FENETRE;
        }

//collisions


//entre meteore 1 et meteore 2
        if (meteore.getGlobalBounds().intersects(meteore2.getGlobalBounds()))
        {

            if (x<=x2)
            {
                x=x-2;
            }
            else
            {
                y2=y2-2;
            }
        }


//entre meteore 1 et meteore 3
        if(meteore.getGlobalBounds().intersects(meteore3.getGlobalBounds()))
        {

            x=x-2;

        }

//entre meteore 2 et meteore 3
        if (meteore2.getGlobalBounds().intersects(meteore3.getGlobalBounds()))
        {

            y2=y2-2;
        }

//entre meteore 1 et meteore 4
        if (meteore.getGlobalBounds().intersects(meteore4.getGlobalBounds()))
        {

            x=x-2;


//entre meteore 2 et meteore 4
        }
        if (meteore2.getGlobalBounds().intersects(meteore4.getGlobalBounds()))
        {

            y2=y2-2;


//entre meteore 3 et 4
        }

        if (meteore3.getGlobalBounds().intersects(meteore4.getGlobalBounds()))
        {

            x3=x3-2;


        }

//entre meteore 1 et 5

        if (meteore.getGlobalBounds().intersects(meteore5.getGlobalBounds()))
        {
            x=x-2;
        }
//entre meteore 2 et 5
        if (meteore2.getGlobalBounds().intersects(meteore5.getGlobalBounds()))
        {
            y2=y2-2;
        }

//entre meteore 3 et 5
        if (meteore3.getGlobalBounds().intersects(meteore5.getGlobalBounds()))
        {
            x3=x3-1;
        }

//entre meteore 4 et 5
        if (meteore4.getGlobalBounds().intersects(meteore5.getGlobalBounds()))
        {
            x4=x4-2;
        }
//entre meteore 1 et 6
        if (meteore.getGlobalBounds().intersects(meteore6.getGlobalBounds()))
        {
            x=x-2;
        }
//entre meteore 2 et 6
        if (meteore2.getGlobalBounds().intersects(meteore6.getGlobalBounds()))
        {
            y2=y2-2;
        }
//entre meteore 3 et 6
        if (meteore3.getGlobalBounds().intersects(meteore6.getGlobalBounds()))
        {
            x3=x3-2;
        }
//entre meteore 4 et 6
        if (meteore4.getGlobalBounds().intersects(meteore6.getGlobalBounds()))
        {
            x4=x4-2;
        }
//entre meteore 5 et 6
        if (meteore.getGlobalBounds().intersects(meteore6.getGlobalBounds()))
        {
            x5=x5-2;
        }
//entre le vaisseau et les meteores







        if(tir1S.getGlobalBounds().intersects(meteore.getGlobalBounds()))
        {
            x=0;
            y=rand()%HAUT_FENETRE;
            meteore.setPosition(x,y);
            isShooting=false;
            tir1S.setPosition(ship.getPosition().x,ship.getPosition().y);


            if(avance==1)
                tir1S.setRotation(ship.getRotation());
            if(tourne==1)
                tir1S.setRotation(ship.getRotation());
            else if(tourne==-1)
                tir1S.setRotation(ship.getRotation());
            score=score+valeur_score;

        }

        if(tir1S.getGlobalBounds().intersects(meteore2.getGlobalBounds()))
        {
            y2=0;
            x2=rand()%LARG_FENETRE;
            meteore.setPosition(x2,y2);
            isShooting=false;
            tir1S.setPosition(ship.getPosition().x,ship.getPosition().y);


            if(avance==1)
                tir1S.setRotation(ship.getRotation());
            if(tourne==1)
                tir1S.setRotation(ship.getRotation());
            else if(tourne==-1)
                tir1S.setRotation(ship.getRotation());
            score=score+valeur_score;

        }


        if(tir1S.getGlobalBounds().intersects(meteore3.getGlobalBounds()))
        {
            x3=rand()%LARG_FENETRE;
            y3=0;
            meteore.setPosition(x3,y3);
            isShooting=false;
            tir1S.setPosition(ship.getPosition().x,ship.getPosition().y);


            if(avance==1)
                tir1S.setRotation(ship.getRotation());
            if(tourne==1)
                tir1S.setRotation(ship.getRotation());
            else if(tourne==-1)
                tir1S.setRotation(ship.getRotation());
            score=score+valeur_score;

        }


        if(tir1S.getGlobalBounds().intersects(meteore4.getGlobalBounds()))
        {
            x4=rand()%LARG_FENETRE;
            y4=HAUT_FENETRE;
            meteore.setPosition(x4,y4);
            isShooting=false;
            tir1S.setPosition(ship.getPosition().x,ship.getPosition().y);


            if(avance==1)
                tir1S.setRotation(ship.getRotation());
            if(tourne==1)
                tir1S.setRotation(ship.getRotation());
            else if(tourne==-1)
                tir1S.setRotation(ship.getRotation());
            score=score+valeur_score;

        }


        if(tir1S.getGlobalBounds().intersects(meteore5.getGlobalBounds()))
        {
            x5=rand()%LARG_FENETRE;
            y5=0;
            meteore.setPosition(x5,y5);
            isShooting=false;
            tir1S.setPosition(ship.getPosition().x,ship.getPosition().y);


            if(avance==1)
                tir1S.setRotation(ship.getRotation());
            if(tourne==1)
                tir1S.setRotation(ship.getRotation());
            else if(tourne==-1)
                tir1S.setRotation(ship.getRotation());
            score=score+valeur_score;

        }


        if(tir1S.getGlobalBounds().intersects(meteore6.getGlobalBounds()))
        {
            y6=HAUT_FENETRE;
            x6=rand()%LARG_FENETRE;
            meteore.setPosition(x6,y6);
            isShooting=false;
            tir1S.setPosition(ship.getPosition().x,ship.getPosition().y);


            if(avance==1)
                tir1S.setRotation(ship.getRotation());
            if(tourne==1)
                tir1S.setRotation(ship.getRotation());
            else if(tourne==-1)
                tir1S.setRotation(ship.getRotation());
            score=score+valeur_score;

        }





        if(ship_position.x!=LARG_FENETRE/2 && ship_position.y!=HAUT_FENETRE/2)
        {

            if (ship.getGlobalBounds().intersects(meteore.getGlobalBounds()))
            {
                ship.setColor(Color::Red);
                ship.setPosition(LARG_FENETRE/2,HAUT_FENETRE/2);
                avance=0;
                vie--;
                printf("%i",vie);
            }
            if (ship.getGlobalBounds().intersects(meteore2.getGlobalBounds()))
            {
                ship.setColor(Color::Red);
                ship.setPosition(LARG_FENETRE/2,HAUT_FENETRE/2);
                vie--;
                avance=0;
                printf("%i",vie);
            }
            if (ship.getGlobalBounds().intersects(meteore3.getGlobalBounds()))
            {
                ship.setColor(Color::Red);
                ship.setPosition(LARG_FENETRE/2,HAUT_FENETRE/2);
                vie--;
                avance=0;
                printf("%i",vie);
            }
            if (ship.getGlobalBounds().intersects(meteore4.getGlobalBounds()))
            {
                ship.setColor(Color::Red);
                ship.setPosition(LARG_FENETRE/2,HAUT_FENETRE/2);
                vie--;
                avance=0;
                printf("%i",vie);
            }
            if (ship.getGlobalBounds().intersects(meteore5.getGlobalBounds()))
            {
                ship.setColor(Color::Red);
                ship.setPosition(LARG_FENETRE/2,HAUT_FENETRE/2);
                vie--;
                avance=0;
                printf("%i",vie);
            }
            if (ship.getGlobalBounds().intersects(meteore6.getGlobalBounds()))
            {
                ship.setColor(Color::Red);
                ship.setPosition(LARG_FENETRE/2,HAUT_FENETRE/2);
                vie--;
                avance=0;
                printf("%i",vie);
            }
        }
        if (vie == 0)
        {
            app.close();
        }


//--WALLPAPER SWITCH----------------------------------------------------
        background.setPosition(-2020,-13);

        app.clear();
        app.draw(background);
        app.draw(ship);
        app.draw(tir1S);

        app.draw(meteore);
        app.draw(meteore2);
        app.draw(meteore3);
        app.draw(meteore4);
        app.draw(meteore5);
        app.draw(meteore6);
        app.display();

        sleep(milliseconds(2));
        meteore.setColor(original);
        meteore2.setColor(original);
        meteore3.setColor(original );
        meteore4.setColor(original);
        meteore5.setColor(original);
        meteore6.setColor(original);
        ship.setColor(original2);


    }


    return EXIT_SUCCESS;
}



float DegreToRad(float degree)
{
    float radian;
    return  radian = (degree * M_PI)/180;
}


