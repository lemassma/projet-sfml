#include <SFML/Graphics.hpp>
#include <time.h>
#define LARG_FEN 1600
#define HAUT_FEN 900
using namespace sf;


int main()
{
    srand(time(NULL));
    sf::RenderWindow app(sf::VideoMode(LARG_FEN,HAUT_FEN), "Space War");
    float x,x2,x3,x4;
    float y,y2,y3,y4;
    int pas=0.25;
    app.clear();
    Texture image;
    Texture background;

//images
    if (!background.loadFromFile("bck.jpg"))
        printf("Pb chargement de l'image");

    if (!image.loadFromFile("comete.png"))
        printf("Pb chargement de l'image");


//fond d'�cran
    Sprite bckground;
    bckground.setTexture(background);
    app.draw(bckground);

//meteore num�ro 1
    Sprite meteore;
    meteore.setTexture(image);
    meteore.setScale(0.6f, 0.6f);
    x=rand()%HAUT_FEN;
    y=rand()%LARG_FEN;
    meteore.setPosition(x,y);



//meteore numero 2
    Sprite meteore2;
    meteore2.setTexture(image);
    meteore2.setScale(0.6f, 0.6f);
    x2=rand()%HAUT_FEN;
    y2=rand()%LARG_FEN;
    meteore2.setPosition(x2,y2);
   ;

//meteore numero 3
    Sprite meteore3;
    meteore3.setTexture(image);
    meteore3.setScale(0.6f, 0.6f);
    x3=0;
    y3=0;
    meteore3.setPosition(x3,y3);


//meteore numero 4
    Sprite meteore4;
    meteore4.setTexture(image);
    meteore4.setScale(0.6f, 0.6f);
    x4=rand()%HAUT_FEN;
    y4=rand()%LARG_FEN;
    meteore4.setPosition(x4,y4);


    app.clear();
    app.display();


//boucle principale
    while (app.isOpen())
    {
        sf::Event event;
        while (app.pollEvent(event))
        {
            if (event.type==Event::Closed)
                app.close();
        }
        app.clear();
        x=x+0.25;
        y2=y2+0.25;
        x3=x3+0.25;
        y3=y3+0.25;
        x4=x4+0.25;
        y4=y4-0.25;
        bckground.setPosition(0,0);
        meteore.setPosition(x,y);
        meteore2.setPosition(x2,y2);
        meteore3.setPosition(x3,y3);
        meteore4.setPosition(x4,y4);
        app.draw(bckground);
        app.draw(meteore);
        app.draw(meteore2);
        app.draw(meteore3);
        app.draw(meteore4);
        app.display();

//quand la com�te sors de l'�cran
        if (x>=LARG_FEN)
        {
            app.clear();


            x=0;
            y=rand()%HAUT_FEN;

        }
        if(y2>HAUT_FEN)
        {
            app.clear();
            y2=0;
            x2=rand()%LARG_FEN;

        }
        if (x3>LARG_FEN || y3>HAUT_FEN)
        {
            app.clear();
            x3=rand()%LARG_FEN;
            y3=0;

        }
        if(x4<0 || y4<=0)
        {
            app.clear();
            x4=rand()%LARG_FEN;
            y4=HAUT_FEN;
        }

//collisions

        if (meteore.getGlobalBounds().intersects(meteore2.getGlobalBounds()))
        {
            app.clear();
            x=x-0.25;

        }
        if(meteore.getGlobalBounds().intersects(meteore3.getGlobalBounds()))
        {
            app.clear();
            x=x-0.25;
        }
        if (meteore2.getGlobalBounds().intersects(meteore3.getGlobalBounds()))
        {
            app.clear();
            y2=y2-0.25;
        }
        if (meteore.getGlobalBounds().intersects(meteore4.getGlobalBounds()))
        {
            app.clear();
            x=x-0.25;

        }
        if (meteore2.getGlobalBounds().intersects(meteore4.getGlobalBounds()))
        {
            app.clear();
            y2=y2-0.25;

        }

        if (meteore3.getGlobalBounds().intersects(meteore4.getGlobalBounds()))
        {
            app.clear();
            x3=x3-0.25;

        }
    }
    return EXIT_SUCCESS;

}



